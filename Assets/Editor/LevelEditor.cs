﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(LevelObject))]
public class LevelEditor : Editor
{
	public bool foldout = true;
	
	public string Name;
	public int[] ReqScores;
	public List<Word> Words;

	public void Awake()
	{
		var target = this.target as LevelObject;
		if (target == null)
			return;
		LoadLevelObject(target);
	}

	public void LoadLevelObject(LevelObject target)
	{
		Name = target.Name;
		ReqScores = target.ReqScores.ToArray();
		Words = new List<Word>(target.Words);
	}

	public void ApplyToLevelObject(LevelObject target)
	{
		target.Name = Name;
		target.ReqScores = ReqScores;
		target.Words = Words;
		EditorUtility.SetDirty(target);
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if (ReqScores == null)
			ReqScores = new[] {60, 80, 95};
		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Reqs");
		for (var i = 0; i < ReqScores.Length; i++)
		{
			GUILayout.Label($"{i + 1}:");
			if(i < ReqScores.Length - 1)
				ReqScores[i] = Math.Min(EditorGUILayout.IntField(ReqScores[i]), ReqScores[i + 1]);
			else ReqScores[i] = EditorGUILayout.IntField(ReqScores[i]);
		}
		EditorGUILayout.EndHorizontal();
		
		foldout = EditorGUILayout.Foldout(foldout, $"Words (Count: {Words.Count})");
		if (foldout)
		{
			for (int i = 0; i < Words.Count; i++)
			{
				EditorGUILayout.BeginHorizontal();
				Words[i] = new Word
				{
					OriginalWord = EditorGUILayout.TextField(Words[i].OriginalWord),
					Translation = EditorGUILayout.TextField(Words[i].Translation)
				};
				if (GUILayout.Button("+")) { Words.Insert(i, new Word()); break; }
				if (GUILayout.Button("X")) { Words.RemoveAt(i); break; }
				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("+"))
				Words.Add(new Word());
			if(GUILayout.Button("Clear"))
				Words.Clear();
			if (GUILayout.Button("Load words"))
			{
				Words.Clear();
				LoadWords();
			}
			EditorGUILayout.EndHorizontal();
		}

		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button("Cancel"))
			LoadLevelObject(this.target as LevelObject);
		if(GUILayout.Button("Apply"))
		{
			var target = this.target as LevelObject;
			ApplyToLevelObject(target);
			LoadLevelObject(target);
		}
		EditorGUILayout.EndHorizontal();
	}

	public void LoadWords()
	{
		var fileName = $"Assets/Levels/ParsingFiles/{Name}.txt";
		if (!File.Exists(fileName))
		{
			Debug.Log($"File {fileName} doesn't exist!");
			return;
		}
			
		var sr = new StreamReader(fileName);

		var curWord = new Word();
		var wasTransq = false;
		while (!sr.EndOfStream)
		{
			var line = ToTitleCase(sr.ReadLine().Trim());
			if(line == "") continue;
			if (curWord.OriginalWord == null)
			{
				curWord.OriginalWord = line;
				continue;
			}

			if (!wasTransq)
			{
				wasTransq = true;
				continue;
			}

			wasTransq = false;
			curWord.Translation = line;
			Words.Add(curWord);
			curWord = new Word();
		}
	}

	public string ToTitleCase(string text)
	{
		if (string.IsNullOrEmpty(text))
			return "";
		return char.ToUpper(text[0]) + string.Concat(text.Skip(1));
	}
}
