﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.SocialPlatforms.Impl;

public class MainGUI : MonoBehaviour
{
	public enum Screens
	{
		MainMenu,
		ResetConfirmation,
		LevelSelection,
		
		LevelInstruction,
		LevelLoop,
		LevelCongratulations
	}

	private string dataPath;
	
	public struct LevelResultInfo
	{
		public double Time;
		public double Score;
	}
	
	public Dictionary<string, LevelResultInfo> playerResults = new Dictionary<string, LevelResultInfo>();

	public Screens currentScreen;
	

	public int selectedLevel = -1;
	public LevelObject[] levels;

	public double timePassed;
	public double timeLastRightClick, timeLastClick;
	private int LastClickId = -1;
	private const double blinkTime = 0.8;
	private const double blinkPeriod = 0.1;
	public double score;
	public double total => score / levels[selectedLevel].Words.Count;

	private bool backPressed;
	

	public int currentWord;
	public ShufledLevel currentLevel;
	
	public GUISkin baseSkin;

	private int bw, bh, fontSize;
	public double fontScale = 0.5;

	public AudioSource ASource;
	public AudioClip RightSound, WrondSound;

	private void Start()
	{
		dataPath = $"{Application.persistentDataPath}/playerSavedData";
		LoadPlayerData();
		print(Application.persistentDataPath);
	}

	private void Update()
	{
		if (Input.touchCount <= 0) deltaTouch = Vector2.zero;
		else deltaTouch = Input.touches[0].deltaPosition;
		backPressed = Input.GetKeyDown(KeyCode.Escape);
		if (currentScreen == Screens.LevelLoop)
			timePassed += Time.deltaTime;
	}

	private Vector2 deltaTouch;
	public Vector2 TouchScrollView(Rect position, Vector2 scrollPosition, Rect viewRect)
	{
		var res = GUI.BeginScrollView(position, scrollPosition, viewRect,
			GUIStyle.none,
			GUIStyle.none);
		if(Input.touchCount <= 0) return res;
		var touch = Input.touches[0];
		if (touch.phase == TouchPhase.Moved)
		{
			res += deltaTouch; deltaTouch = Vector2.zero;
		}

		return res;
	}

	public int GetStars(LevelObject level)
	{
		var stars = 0;
		var score = GetResult(level).Score;
		for (int i = 0; i < level.ReqScores.Length; i++)
		{
			if (score >= level.ReqScores[i]) stars++;
			else break;
		}

		return stars;
	}

	public LevelResultInfo GetResult(LevelObject level)
	{
		playerResults.TryGetValue(level.Name, out var result);
		return result;
	}

	public int GetGlobalStars() => levels.Sum(GetStars);
	
	public double GetGlobalScore() => levels.Sum(l => GetResult(l).Score);

	public void DeletePlayerData()
	{
		ResetPlayerData();
		if(File.Exists(dataPath)) File.Delete(dataPath);
	}

	public void ResetPlayerData()
	{
		playerResults.Clear();
	}

	public void SavePlayerData()
	{
		var sw = new StreamWriter(dataPath);
		foreach (var result in playerResults)
		{
			sw.WriteLine($"{result.Key}|{result.Value.Score}|{result.Value.Time}");
		}
		sw.Close();
		Debug.Log($"Saved to {dataPath}");
	}

	public void LoadPlayerData()
	{
		if(!File.Exists(dataPath))
			return;
		
		playerResults.Clear();
		
		var sr = new StreamReader(dataPath);
		while(!sr.EndOfStream)
		{
			var line = sr.ReadLine();
			if(string.IsNullOrEmpty(line))
				continue;
			var words = line.Split('|');
			if(words.Length != 3 || !double.TryParse(words[1], out var lScore) || !double.TryParse(words[2], out var lTime))
				continue;
			playerResults[words[0]] = new LevelResultInfo {Score = lScore, Time = lTime};
		}
		sr.Close();
	}

	private void OnGUI()
	{
		GUI.skin = baseSkin;
		
		var texw = GUI.skin.button.normal.background.width;
		var texh = GUI.skin.button.normal.background.height;

		var ratio = (double) texh / texw;
		var dwidth = Screen.width * 0.95;
		var dheight = dwidth * ratio;

		bw = (int) dwidth;
		bh = (int) dheight;
		fontSize = (int) (bh * fontScale);

		if (currentScreen == Screens.MainMenu)
			DrawMainMenu();
		else if (currentScreen == Screens.ResetConfirmation)
			DrawResetConfirmationMenu();
		else if (currentScreen == Screens.LevelSelection)
			DrawLevelSelectionMenu();
		else if (currentScreen == Screens.LevelInstruction)
			DrawLevelInstructionMenu();
		else if (currentScreen == Screens.LevelLoop)
			DrawLoopMenu();
		else if (currentScreen == Screens.LevelCongratulations)
			DrawCongratulationsMenu();
	}

	private void DrawMainMenu()
	{
		var cy = 2 + bh;
		SmallStars(1, 1, new Vector2(
				(Screen.width - (Screen.width - bw) / 2) - 3 * (starsSize + 2) - (bw / 40),
				cy - bh + (bh - starsSize) / 2),
			$"{GetGlobalStars()}/{3 * levels.Length}");
		
		cy = (Screen.height - bh) / 2;
		if (SimpleButton("Начать", ref cy))
			currentScreen = Screens.LevelSelection;
		cy += 2;
		if (SimpleButton("Сбросить прогресс", ref cy))
			currentScreen = Screens.ResetConfirmation;
	}

	private void DrawResetConfirmationMenu()
	{
		var cy = (Screen.height - bh) / 2;
		SimpleBox("Вы уверены, что хотите сбросить весь прогресс?", ref cy);
		cy += 2;
		
		if (GUI.Button(new Rect((Screen.width - bw) / 2, cy, bw / 2 - 1, bh), "Сбросить",
			new GUIStyle(GUI.skin.button) {fontSize = fontSize}) || backPressed)
		{
			DeletePlayerData();
			currentScreen = Screens.MainMenu;
		}

		if (GUI.Button(new Rect((Screen.width - bw) / 2 + bw / 2 + 1, cy, bw / 2 - 1, bh), "Отмена",
			new GUIStyle(GUI.skin.button) {fontSize = fontSize}))
			currentScreen = Screens.MainMenu;
	}

	private Vector2 ScrollLevelSelection;
	void DrawLevelSelectionMenu()
	{
		var cy = 2;
		
		DrawHeaderBox("Выбор уровня", ref cy);
		SmallStars(1, 1, new Vector2(
				(Screen.width - (Screen.width - bw) / 2) - 3 * (starsSize + 2) - (bw / 40),
				cy - bh + (bh - starsSize) / 2),
			$"{GetGlobalStars()}/{3 * levels.Length}");
		cy += 2;

		ScrollLevelSelection = TouchScrollView(
			new Rect(0, cy, Screen.width + 20, Screen.height - cy - bh - 2),
			ScrollLevelSelection,
			new Rect(Vector2.zero, new Vector2(Screen.width, levels.Length * (bh + 2))));

		cy = 0;

		var globalScore = GetGlobalScore();
		for (var i = 0; i < levels.Length; i++)
		{
			if (LevelButton(i, ref cy))
			{
				selectedLevel = i;
				currentScreen = Screens.LevelInstruction;
				return;
			}

			cy += 2;
		}
		
		GUI.EndScrollView();

		cy = Screen.height - bh - 2;

		if (SimpleButton("Назад", ref cy) || backPressed)
		{
			currentScreen = Screens.MainMenu;
			backPressed = false;
		}
	}

	public Texture2D LevelHeaderTexture;
	private Vector2 ScrollLevelInstruction;
	public void DrawLevelInstructionMenu()
	{
		var cy = 2;
		var hasRecord = playerResults.TryGetValue(levels[selectedLevel].Name, out var result);

		ScrollLevelInstruction = TouchScrollView(
			new Rect(0, cy, Screen.width, Screen.height - cy - bh - 4),
			ScrollLevelInstruction,
			new Rect(Vector2.zero, new Vector2(Screen.width,
				(levels[selectedLevel].Words.Count + 2) * (bh + 2) + 4)));

		cy = 0;
		
		var gs = new GUIStyle(GUI.skin.box);
		gs.normal.background = LevelHeaderTexture;
		gs.normal.textColor = GUI.skin.button.normal.textColor;
		
		SimpleBox(levels[selectedLevel].Name, ref cy, gs);
		cy += 2;
		
		var scoreString = hasRecord ? result.Score.ToString("f2") : "---";
		var timeString = hasRecord ? result.Time.ToString("f2") : "---";
		SimpleBox($"Очки: {scoreString}, Время: {timeString}", ref cy, gs);
		cy += 2;

		cy += 4;

		foreach (var word in levels[selectedLevel].Words)
		{
			DrawWordInstructionBox(word, ref cy);
			cy += 2;
		}

		GUI.EndScrollView();
		
		cy = Screen.height - bh - 2;

		if (GUI.Button(new Rect((Screen.width - bw) / 2, cy, bw / 2 - 1, bh), "Назад",
			new GUIStyle(GUI.skin.button) {fontSize = fontSize}) || backPressed)
		{
			selectedLevel = -1;
			currentScreen = Screens.LevelSelection;
			backPressed = false;
		}

		if (GUI.Button(new Rect((Screen.width - bw) / 2 + bw / 2 + 1, cy, bw / 2 - 1, bh), "Начать",
			new GUIStyle(GUI.skin.button) {fontSize = fontSize}))
		{
			currentScreen = Screens.LevelLoop;
			currentLevel = levels[selectedLevel].Instantiate().Shufle();
			timePassed = timeLastRightClick = 0;
			score = 0;
		}
	}

	private byte wrongSelections;

	byte wrongs
	{
		get
		{
			byte result = 0;
			for (int i = 0; i < 8; i++)
				if ((wrongSelections >> i & 1) == 1)
					result++;
			return result;
		}
	}
	
	public void DrawLoopMenu()
	{
		if (currentWord >= currentLevel.Words.Count)
		{
			currentLevel = null;
			currentWord = 0;
			currentScreen = Screens.LevelCongratulations;
			return;
		}
		
		var cy = 2;


		var starsFilled = 0;
		foreach (var reqScore in levels[selectedLevel].ReqScores)
		{
			if (total >= reqScore)
				starsFilled++;
		}
		
		BigStars(levels[selectedLevel].ReqScores.Length, starsFilled, ref cy, total.ToString("f0"));
		cy += 2;
		

		var word = currentLevel.Words[currentWord];
		var selWordId = ShufledWordSelection(word, ref cy);

		if (LastClickId == word.CorrectTranslationId)
		{
			if (timePassed - timeLastClick > blinkTime)
			{
				var dt = timeLastClick - timeLastRightClick + wrongs * 1.5;
				score += Math.Max(20 - wrongs * 5, 100 - dt * dt * dt);
				timeLastRightClick = timePassed;
				currentWord++;
				wrongSelections = 0;
				LastClickId = -1;
			}
		}
		else if(selWordId == word.CorrectTranslationId)
		{
			timeLastClick = timePassed;
			LastClickId = selWordId;
			ASource.PlayOneShot(RightSound);
		}
		else if(selWordId != -1)
		{
			timeLastClick = timePassed;
			LastClickId = selWordId;
			wrongSelections |= (byte) (1 << selWordId);
			ASource.PlayOneShot(WrondSound);
		}
		
		cy = Screen.height - bh - 2;

		if (SimpleButton("К выбору уровня", ref cy) || backPressed)
		{
			LastClickId = -1;
			selectedLevel = -1;
			currentLevel = null;
			currentScreen = Screens.LevelSelection;
			backPressed = false;
		}
	}

	public void DrawCongratulationsMenu()
	{
		var cy = 2;

		var starsFilled = 0;
		foreach (var reqScore in levels[selectedLevel].ReqScores)
		{
			if (total >= reqScore)
				starsFilled++;
		}
		BigStars(levels[selectedLevel].ReqScores.Length, starsFilled, ref cy);
		cy += 2;

		var hasRecord = playerResults.TryGetValue(levels[selectedLevel].Name, out var prevRecord);
		DrawResultBox($"Очки: {total:0.00}" + (total > prevRecord.Score ? " (новый рекорд!)" : ""), ref cy);
		cy += 2;
		DrawResultBox($"Время: {timePassed:0.00}" + (timePassed < prevRecord.Time ? " (новый рекорд!)" : ""), ref cy);
		cy = Screen.height - bh - 2;

		if (SimpleButton("К выбору уровня", ref cy) || backPressed)
		{
			if (total > prevRecord.Score || timePassed < prevRecord.Time)
			{
				var newScore = hasRecord ? Math.Max(total, prevRecord.Score) : total;
				var newTime = hasRecord ? Math.Min(timePassed, prevRecord.Time) : timePassed;
				playerResults[levels[selectedLevel].Name] = new LevelResultInfo {Score = newScore, Time = newTime};
				SavePlayerData();
			}

			selectedLevel = -1;
			currentLevel = null;
			currentScreen = Screens.LevelSelection;
			backPressed = false;
		}
	}

	public void DrawHeaderBox(string text, ref int cy) => SimpleBox(text, ref cy);
	public void DrawResultBox(string text, ref int cy) => SimpleBox(text, ref cy);

	public Texture2D emptyBigStar, filledBigStar;
	public void BigStars(int count, int filled, ref int cy, string text = null)
	{
		var ebsw = emptyBigStar.width + 10;
		var position = new Vector2((Screen.width - 3 * ebsw) / 2, cy);
		for (int i = 0; i < count; i++)
		{
			if(i < filled) GUI.DrawTexture(new Rect(position + i * Vector2.right * ebsw, 
					new Vector2(filledBigStar.width, filledBigStar.height)), filledBigStar);
			else GUI.DrawTexture(new Rect(position + i * Vector2.right * ebsw, 
				new Vector2(emptyBigStar.width, emptyBigStar.height)), emptyBigStar);
		}

		if (text != null)
		{
			GUI.Label(new Rect(0, cy + emptyBigStar.height - 24, Screen.width, 48), text);
			cy += 24;
		}

		cy += emptyBigStar.height;
	}

	public Texture2D emptySmallStar, filledSmallStar;
	public GUIStyle smallStarsLabelStyle;
	[FormerlySerializedAs("starscoef")] public double starsCoef = 0.6;
	private int starsSize => (int) (bh * starsCoef);
	
	public void SmallStars(int count, int filled, Vector2 position, string rightText = null)
	{
		// var ebsw = bh - 30 + 2;
		for (int i = 0; i < count; i++)
		{
			if(i < filled)
				GUI.DrawTexture(new Rect(position + i * Vector2.right * (starsSize + 2), 
					new Vector2((int) (bh * starsCoef), (int) (bh * starsCoef))), filledSmallStar);
			else GUI.DrawTexture(new Rect(position + i * Vector2.right * (starsSize + 2), 
				new Vector2(starsSize, (int) (bh * starsCoef))), emptySmallStar);
		}

		smallStarsLabelStyle.fontSize = starsSize;
		if(rightText != null)
			GUI.Label(new Rect(position + count * Vector2.right * (starsSize + 2), 
				new Vector2((int) (bh * starsCoef), (int) (bh * starsCoef))), rightText, smallStarsLabelStyle);
	}

	public int ShufledWordSelection(ShufledWord word, ref int cy)
	{
		DrawWordBox(word.OriginalWord, ref cy);
		cy += 2;
		for (var i = 0; i < word.Translations.Count; i++)
		{
			if ((wrongSelections >> i & 1) == 1)
			{
				ShufledWordWrongTranslationBox(word, i, ref cy);
				cy += 2;
				continue;
			}
			if (ShufledWordTranslationButton(word, i, ref cy))
				return i;
			cy += 2;
		}

		return -1;
	}

	public Texture2D rightTranslationTexture;
	public Texture2D normalTranslationTexture;
	public Texture2D wrondTranslationTexture;
	public bool ShufledWordTranslationButton(ShufledWord word, int translation, ref int cy)
	{
		if (LastClickId == translation && timePassed - timeLastClick < blinkTime)
		{
			var styleBox = new GUIStyle(GUI.skin.box);
			styleBox.normal.background = rightTranslationTexture;
			SimpleBox(word.Translations[translation], ref cy, styleBox);
			return false;
		}
		
		var style = new GUIStyle(GUI.skin.button);
		style.normal.background = normalTranslationTexture;
		style.normal.textColor = GUI.skin.box.normal.textColor;
		return SimpleButton(word.Translations[translation], ref cy, style);
	}

	public void ShufledWordWrongTranslationBox(ShufledWord word, int translation, ref int cy)
	{
		var style = new GUIStyle(GUI.skin.box);
		var lcDelta = timePassed - timeLastClick;
		style.normal.background = ((LastClickId == translation) && (lcDelta <= blinkTime)
		                                                        && ((lcDelta % (2 * blinkPeriod)) < (blinkPeriod)))
		 ? normalTranslationTexture : wrondTranslationTexture;
		SimpleBox(word.Translations[translation], ref cy, style);
	}

	public Texture2D wordBoxTexture;
	public void DrawWordBox(string word, ref int cy)
	{
		var style = new GUIStyle(GUI.skin.box);
		style.normal.background = wordBoxTexture;
		style.normal.textColor = GUI.skin.button.normal.textColor;
		style.fontSize = bh;
		SimpleBox(word, ref cy, style);
	}

	public bool LevelButton(int levelId, ref int cy)
	{
		var ccy = cy;
		var curRecord = GetResult(levels[levelId]);
		var result = false;
		var globalStars = GetGlobalStars();
		if(globalStars >= levels[levelId].StarsToUnlock) result = SimpleButton(levels[levelId].Name, ref ccy);
		else
		{
			SimpleBox(levels[levelId].Name, ref ccy);
			SmallStars(1, 1,
				new Vector2(
					(Screen.width - (Screen.width - bw) / 2) - 3 * (starsSize + 2) - (bw / 40),
					cy + ((bh -  starsSize) / 2)),
				$"{globalStars}/{levels[levelId].StarsToUnlock}");
		}
		if(curRecord.Score > 0)
		{
			var starsCount = levels[levelId].ReqScores.Length;
			var starsFilled = 0;
			foreach (var reqScore in levels[levelId].ReqScores)
			{
				if (curRecord.Score >= reqScore)
					starsFilled++;
			}
			SmallStars(starsCount, starsFilled,
				new Vector2(
					(Screen.width - (Screen.width - bw) / 2) - 3 * (starsSize + 2) - (bw / 40),
					cy + ((bh -  starsSize) / 2)));
		}
		
		cy += bh;
		return result;
	}

	public void DrawWordInstructionBox(Word word, ref int cy)
		=> SimpleBox($"<b>{word.OriginalWord}</b> - {word.Translation}", ref cy);

	public void SimpleBox(string text, ref int cy, GUIStyle style = null)
	{
		if (style == null) style = new GUIStyle(GUI.skin.box);
		style.fontSize = fontSize;
		var ratio = 0.95 * bw / style.CalcSize(new GUIContent(text)).x;
		if (ratio > 1) ratio = 1;
		style.fontSize = (int) (fontSize * ratio);
		GUI.Box(new Rect((Screen.width - bw) / 2, cy, bw, bh), text, style);
		cy += bh;
	}

	public bool SimpleButton(string text, ref int cy, GUIStyle style = null)
	{
		if(style == null) style = new GUIStyle(GUI.skin.button);
		style.fontSize = fontSize;
		var ratio = 0.95 * bw / style.CalcSize(new GUIContent(text)).x;
		if (ratio > 1) ratio = 1;
		style.fontSize = (int) (fontSize * ratio);
		var result = GUI.Button(new Rect((Screen.width - bw) / 2, cy, bw, bh), text, style);
		cy += bh;
		return result;
	}
}