﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "Level", menuName = "Create level", order = 1)]
public class LevelObject : ScriptableObject
{
	public string Name;
	public int StarsToUnlock;
	[HideInInspector]
	public int[] ReqScores;
    [HideInInspector]
    public List<Word> Words = new List<Word>();

    public Level Instantiate()
    {
	    return new Level(Name, ReqScores,new List<Word>(Words), StarsToUnlock);
    }
}

public class Level
{
	public string Name;
	public int[] ReqScores;
	public int StarsToUnlock = 0;
	public List<Word> Words;

	public Level(string name, int[] reqScores, List<Word> words, int starsToUnlock)
	{
		Name = name;
		ReqScores = reqScores;
		Words = words;
		StarsToUnlock = starsToUnlock;
	}

	public ShufledLevel Shufle()
	{
		var result = new ShufledLevel();
		foreach (var word in Words)
		{
			var sWord = new ShufledWord();
			sWord.OriginalWord = word.OriginalWord;
			sWord.Translations = new List<string>();
			sWord.Translations.Add(word.Translation);
			while (sWord.Translations.Count < 4)
			{
				var insertIndex = Random.Range(0, sWord.Translations.Count + 1);
				
				var randonWordId = Random.Range(0, Words.Count);
				while (sWord.Translations.Any(t => t == Words[randonWordId].Translation))
					randonWordId = Random.Range(0, Words.Count);
				
				sWord.Translations.Insert(insertIndex, Words[randonWordId].Translation);
			}

			sWord.CorrectTranslationId = sWord.Translations.IndexOf(word.Translation);
			var insertWordIndex = Random.Range(0, result.Words.Count + 1);
			result.Words.Insert(insertWordIndex, sWord);
		}

		return result;
	}
}

public class ShufledLevel
{
	public List<ShufledWord> Words = new List<ShufledWord>();
}

public struct ShufledWord
{
	public string OriginalWord;
	public int CorrectTranslationId;
	public List<string> Translations;
}

[Serializable]
public struct Word
{
	public string OriginalWord;
	public string Translation;

	public Word(string originalWord, string translation)
	{
		OriginalWord = originalWord;
		Translation = translation;
	}
}