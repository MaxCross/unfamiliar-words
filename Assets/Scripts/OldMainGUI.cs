﻿// using System;
// using System.Collections;
// using System.Collections.Generic;
// using System.Globalization;
// using System.IO;
// using System.Linq;
// using UnityEngine;
// using Random = UnityEngine.Random;
//
// public class MainGUI : MonoBehaviour
// {
//     public GUISkin skin;
//     public GUIStyle headerStyle;
//     public GUIStyle levelButtonStyle;
//     
//     public Texture2D backroundTexture;
//     public Texture2D emptyStarTexture, filledStarTexture;
//     public int selectedLevel = -1;
//
//     public Level[] levels;
//     public LevelResult[] results;
//
//     public double curScore, curTime;
//
//     private Stack<Word> _words;
//     
//     private float _transition;
//     
//     private void Start()
//     {
//         print(Application.persistentDataPath);
//         GUIHelper.EmptyStarTexture = emptyStarTexture;
//         GUIHelper.FilledStarTexture = filledStarTexture;
//         GUIHelper.HeaderStyle = headerStyle;
//         GUIHelper.LevelButtonStyle = levelButtonStyle;
//         
//         if (!Directory.Exists($"{Application.persistentDataPath}/Levels"))
//             Directory.CreateDirectory($"{Application.persistentDataPath}/Levels");
//         levels = Level.FromFiles($"{Application.persistentDataPath}/Levels").ToArray();
//         results = new LevelResult[levels.Length];
//
//         if (!File.Exists($"{Application.persistentDataPath}/playerData"))
//         {
//             var f = File.CreateText($"{Application.persistentDataPath}/playerData");
//             f.Close();
//         }
//         
//         foreach (var levelResult in LevelResult.FromFile($"{Application.persistentDataPath}/playerData"))
//         {
//             print(levelResult.LevelName);
//             var id = GetLevelIDByName(levelResult.LevelName);
//             if(id == -1) continue;
//             print(id);
//             results[id] = levelResult;
//         }
//     }
//     
//     private void Update()
//     {
//         if (curTime >= 0)
//             curTime += Time.deltaTime;
//     }
//
//     public int GetLevelIDByName(string name)
//     {
//         for (var i = 0; i < levels.Length; i++)
//             if (levels[i].Name == name)
//                 return i;
//         return -1;
//     }
//
//     private int _correctAnswer = -1;
//     private int _correctStreak;
//     private Vector2 _scrollPosition;
//     public double prevTime;
//     private void OnGUI()
//     {
//         var curY = 0;
//         GUI.skin = skin;
//         GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backroundTexture);
//
//         if (selectedLevel < 0)
//         {
//             GUIHelper.TitleBar("Выберите уровень", ref curY);
//             
//             _scrollPosition = GUI.BeginScrollView(
//                 new Rect(0, curY, Screen.width, Screen.height - curY), _scrollPosition,
//                 new Rect(0, 0, Screen.width - 20, levels.Length * (GUIHelper.ButtonSize + GUIHelper.Margin) - GUIHelper.Margin),
//                 false, true);
//             curY = 0;
//             
//             for (var i = 0; i < levels.Length; i++)
//                 if (GUIHelper.LevelButton(levels[i], results[i]?.Score ?? -1, ref curY))
//                 {
//                     selectedLevel = i;
//                     _correctStreak = 0;
//                     curTime = -1;
//                 }
//             
//             GUI.EndScrollView();
//         }
//         else if (curTime < 0)
//         {
//             GUIHelper.TitleBar(levels[selectedLevel].Name, ref curY);
//
//             _scrollPosition = GUI.BeginScrollView(
//                 new Rect(0, curY, Screen.width, Screen.height - curY - (GUIHelper.ButtonSize + 2 * GUIHelper.Margin)), _scrollPosition,
//                 new Rect(0, 0, Screen.width - 20, levels[selectedLevel].Words.Count * (GUIHelper.ButtonSize + GUIHelper.Margin) - GUIHelper.Margin),
//                 false, true);
//             curY = 0;
//             
//             foreach (var word in levels[selectedLevel].Words)
//                 GUIHelper.WordTranslationElement(word, ref curY);
//             
//             GUI.EndScrollView();
//             curY = Screen.height - (GUIHelper.ButtonSize + GUIHelper.Margin);
//             if (GUIHelper.Button("Начать", ref curY))
//             {
//                 curTime = 0;
//                 prevTime = -10;
//                 _correctStreak = 0;
//                 curScore = 0;
//                 StartGame();
//             }
//         }
//         else if (_words?.Count > 0)
//         {
//             GUIHelper.TitleBar(levels[selectedLevel].Name, ref curY);
//
//             GUIHelper.Stars(curScore, levels[selectedLevel], ref curY);
//             
//             const int maxDeltaTime = 5;
//             var deltaTime = Math.Min(curTime - prevTime, maxDeltaTime);
//             var deltaTimeMod =
//                 ((maxDeltaTime - deltaTime) / maxDeltaTime) * ((maxDeltaTime - deltaTime) / maxDeltaTime) * ((maxDeltaTime - deltaTime) / maxDeltaTime) * ((maxDeltaTime - deltaTime) / maxDeltaTime) * 5;
//             var streakMod = _correctStreak * 0.5;
//             
//             GUIHelper.Label(
//                 $"Бонус за время: {deltaTimeMod.ToString("0.00", CultureInfo.InvariantCulture)}",
//                 ref curY);
//             GUIHelper.Label(
//                 $"Бонус за серию: {streakMod.ToString("0.00", CultureInfo.InvariantCulture)}",
//                 ref curY);
//             
//             GUIHelper.Label(_words.Peek().OriginalWord, ref curY);
//
//             if(_correctAnswer == -1)
//                 _correctAnswer = Random.Range(0, _words.Peek().Variants.Length + 1);
//
//             for (int i = 0; _words.Count > 0 && i <= _words.Peek().Variants.Length; i++)
//             {
//                 var n = i;
//                 if (n == _correctAnswer)
//                 {
//                     if (GUIHelper.Button(_words.Peek().Translation, ref curY))
//                     {
//                         curScore += 1 + streakMod + deltaTimeMod; 
//                         _words.Pop();
//                         _correctAnswer = -1;
//
//                         prevTime = curTime;
//                         if(_correctStreak < 10)
//                             _correctStreak++;
//                     }
//                     continue;
//                 }
//                 else if (n > _correctAnswer)
//                     n--;
//                 if (GUIHelper.Button(_words.Peek().Variants[n], ref curY))
//                 {
//                     curScore--;
//                     _correctStreak = 0;
//                     prevTime = -10;
//                 }
//             }
//         }
//         else
//         {
//             GUIHelper.TitleBar(levels[selectedLevel].Name, ref curY);
//             GUIHelper.Stars(curScore, levels[selectedLevel], ref curY);
//             if(results[selectedLevel] == null || results[selectedLevel].Score < curScore)
//                 GUIHelper.Label("Новый рекорд!", ref curY);
//             
//             curY = Screen.height - (GUIHelper.ButtonSize + GUIHelper.Margin);
//             if(GUIHelper.Button("К выбору уровня", ref curY))
//             {
//                 if(results[selectedLevel] == null || results[selectedLevel].Score < curScore)
//                     results[selectedLevel] = new LevelResult(levels[selectedLevel].Name, curScore, curTime);
//                 selectedLevel = -1;
//                 LevelResult.Save(results, $"{Application.persistentDataPath}/playerData");
//             }
//         }
//     }
//     
//     public void StartGame()
//     {
//         var tempWords = new LinkedList<Word>(levels[selectedLevel].Words);
//         _words = new Stack<Word>();
//         for (var c = tempWords.Count; c > 0; c--)
//         {
//             var n = Random.Range(0, c);
//             _words.Push(PopAt(tempWords, n));
//         }
//
//         Word PopAt(LinkedList<Word> list, int index)
//         {
//             var cur = list.First;
//             for (; index > 0; index--)
//                 cur = cur.Next;
//             list.Remove(cur);
//             return cur.Value;
//         }
//     }
//
//     public static class GUIHelper
//     {
//         public const int HeaderSize = 100;
//         public const int ButtonSize = 80;
//         public const int LabelSize = 60;
//         public const int StarSize = 160;
//
//         public const int Margin = 2;
//         
//         public static GUIStyle HeaderStyle;
//         public static GUIStyle LevelButtonStyle;
//         public static Texture2D EmptyStarTexture, FilledStarTexture;
//         
//         public static void TitleBar(string text, ref int curY)
//         {
//             GUI.Box(new Rect(0, curY, Screen.width, HeaderSize), text, HeaderStyle);
//             curY += HeaderSize + Margin;
//         }
//
//         public static bool LevelButton(Level level, double curPoints, ref int curY)
//         {
//             var result = GUI.Button(new Rect(2, curY, Screen.width - 2 * Margin - 20, ButtonSize), level.Name, LevelButtonStyle);
//             
//             for (int i = 0; i < level.ReqScores.Length; i++)
//             {
//                 GUI.DrawTexture(
//                     new Rect((Screen.width - ButtonSize * (level.ReqScores.Length)) + ButtonSize * i - 20 - 2 * Margin, curY, ButtonSize, ButtonSize),
//                     curPoints >= level.ReqScores[i] ? FilledStarTexture : EmptyStarTexture);
//             }
//             
//             curY += ButtonSize + Margin;
//             return result;
//         }
//         
//         public static void WordTranslationElement(Word word, ref int curY)
//         {
//             GUI.Box(new Rect(2, curY, Screen.width - 2 * Margin - 20, ButtonSize),
//                 $"<B>{word.OriginalWord}</B> - {word.Translation}");
//             curY += ButtonSize + Margin;
//         }
//         
//         public static void Label(string text, ref int curY)
//         {
//             GUI.Label(new Rect(2, curY, Screen.width - 2 * Margin - 20, LabelSize), text);
//             curY += LabelSize + Margin;
//         }
//
//         public static bool Button(string text, ref int curY)
//         {
//             var result = GUI.Button(new Rect(2, curY, Screen.width - 2 * Margin, ButtonSize), text);
//             curY += ButtonSize + Margin;
//             return result;
//         }
//
//         public static void Stars(double curPoints, Level level, ref int curY)
//         {
//             for (int i = 0; i < level.ReqScores.Length; i++)
//             {
//                 GUI.DrawTexture(
//                     new Rect((Screen.width - StarSize * (level.ReqScores.Length)) / 2f + StarSize * i, curY, StarSize, StarSize),
//                     curPoints >= level.ReqScores[i] ? FilledStarTexture : EmptyStarTexture);
//             }
//             
//             GUI.Label(new Rect(2, curY + StarSize - LabelSize/2, Screen.width - 2 * Margin, LabelSize), curPoints.ToString("0.00", CultureInfo.InvariantCulture));
//
//             curY += StarSize + LabelSize/2 + Margin;
//         }
//     }
// }
//
// public class LevelResult
// {
//     public string LevelName;
//     public double Score;
//     public double TimeInSeconds;
//
//     public LevelResult(string levelName, double score, double timeInSeconds)
//     {
//         LevelName = levelName;
//         Score = score;
//         TimeInSeconds = timeInSeconds;
//     }
//
//     private LevelResult() { }
//
//     public static IEnumerable<LevelResult> FromFile(string fileName)
//     {
//         var sr = new StreamReader(fileName);
//         while (!sr.EndOfStream)
//         {
//             var current = new LevelResult();
//
//             current.LevelName = sr.ReadLine();
//             var score = sr.ReadLine().Split(';');
//             current.Score = double.Parse(score[0]);
//             current.TimeInSeconds = double.Parse(score[1]);
//             
//             yield return current;
//         }
//     }
//
//     public static void Save(IEnumerable<LevelResult> results, string fileName)
//     {
//         var sw = new StreamWriter(fileName);
//         foreach (var result in results)
//         {
//             if(result == null)
//                 continue;
//             sw.WriteLine(result.LevelName);
//             sw.WriteLine($"{result.Score};{result.TimeInSeconds}");
//         }
//         sw.Close();
//     }
// }
//
// public class Level
// {
//     public string Name;
//     public double[] ReqScores;
//     public List<Word> Words = new List<Word>();
//
//     public static IEnumerable<Level> FromFiles(string path, string pattern = "*.level")
//     {
//         var levels = Directory.GetFiles(path, pattern);
//         foreach (var level in levels)
//             yield return FromFile(level);
//     }
//     
//     public static Level FromFile(string fileName)
//     {
//         var result = new Level();
//         var sr = new StreamReader(fileName);
//         result.Name = sr.ReadLine();
//         
//         result.ReqScores = sr.ReadLine().Split(',').Select(double.Parse).ToArray();
//         while (!sr.EndOfStream)
//         {
//             var line = sr.ReadLine().Split('|');
//             var word = line[0].Split('-');
//             var variants = line[1].Split('/');
//             result.Words.Add(new Word(word[0], word[1], variants));
//         }
//         return result;
//     }
// }
//
// public struct Word
// {
//     public string OriginalWord;
//     public string Translation;
//     public string[] Variants;
//
//     public Word(string originalWord, string translation, string[] variants)
//     {
//         OriginalWord = originalWord;
//         Translation = translation;
//         Variants = variants;
//     }
// }